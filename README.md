<div align="center">
  <h1 align="center">
    Simple Unit App
  </h1>
  <img width="200" src="./docs/screenshot.png" alt="screenshot" />
</div>

### Introduction

[natadeCOCO](https://natade-coco.com)では、1 つのコンテナイメージにパッケージングした Web アプリを、[配信ユニット](https://natade-coco.com/product/unit/)にアップロードすることができます。アップロード時に発行されたコードを[専用のモバイルアプリ](https://natade-coco.com/product/pocket/)で読み込むと、配信ユニットに紐づけた Wi-Fi スポットに接続し、モバイルアプリ内のブラウザで Web アプリを開きます。

### Quick Start (10 minutes)

1. [管理コンソール](https://natade-coco.com/product/console/)にログインして配信ユニットを有効にする。
2. 配信ユニットに Web アプリを登録する。

```
タイトル: 任意
イメージ: registry.gitlab.com/natade-coco/apps/welcome:latest
ポート番号: 3030
```

### Development

```
$ cd app/server
$ npm install
$ npm run develop
```

## License

Licensed under the [MIT License](./LICENSE).
