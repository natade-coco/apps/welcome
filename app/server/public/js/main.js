const perspective = document.getElementById("perspective");
const app = document.getElementById("app");
const dimensions = [
  "perspective(500px) rotateX(45deg) rotateZ(45deg)",
  "perspective(500px) rotateX(0deg) rotateZ(0deg) scale3d(2, 2, 2)",
  "perspective(500px) rotateZ(90deg) rotateY(-90deg) scale3d(2, 2, 2)",
  "perspective(500px) rotateZ(0deg) rotateX(90deg) scale3d(2, 2, 2)",
  "perspective(500px) rotateX(90deg) rotateZ(270deg) scale3d(2, 2, 2)",
  "perspective(500px) rotateX(90deg) rotateZ(180deg) scale3d(2, 2, 2)",
  "perspective(500px) rotateX(180deg) rotateZ(180deg) scale3d(2, 2, 2)",
];
const colors = [
  "#09B4C6",
  "#096AC4",
  "#AE09C4",
  "#C43E09",
  "#C48909",
  "#C4AE09",
  "#8FC409",
];
let cnt = 1;
let translateZ = 3;

perspective.onclick = () => {
  perspective.style.transform = dimensions[cnt];
  app.style.background = colors[cnt];
  createTwitterButton(colors[cnt]);
  cnt++;
  if (cnt > 6) cnt = 0;
};

const tweetArea = document.getElementById("twitter");
const createTwitterButton = (color) => {
  if (tweetArea.firstElementChild) {
    tweetArea.removeChild(tweetArea.firstElementChild);
  }
  twttr.widgets.createShareButton(" ", tweetArea, {
    size: "large",
    text: `アプリを配信しました🎉 好きな色は【${color}】です #natadeCOCO`,
  });
};
createTwitterButton(colors[0]);
